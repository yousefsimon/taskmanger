<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\IndexController; 
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController; 
use App\Http\Controllers\CategoriesController; 
use App\Http\Controllers\ProductController; 
use App\Http\Controllers\AttributeController; 

use App\Http\Controllers\AttributeValueController; 
use App\Http\Controllers\DashboardController; 
use App\Http\Controllers\ProductPackController; 
use App\Http\Controllers\OrderController; 
 
use App\Http\Controllers\MessageController; 
use App\Http\Controllers\SubProductController; 
use App\Http\Controllers\DepartmentController; 
use App\Http\Controllers\TaskController; 
use App\Http\Controllers\EmpDashboardController; 
 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
 return redirect('/admin');
});
 */
Route::get('/', [IndexController::class, 'index'])->name('page');

 
 
Route::post('/admin',[LoginController::class,'adminLogin'])->name('admin.login');


	Route::get('/admin/dashboard',[DashboardController::class,'show'])->name('admin.show');




	Route::group(['middleware' => ['auth:admin']], function () {
		Route::get('/admin',[DashboardController::class,'show'])->name('admin.login-view');
  
		Route::get('admin/create', [AdminController::class, 'create'])->name('admin.create');
		Route::post('admin/store', [AdminController::class, 'store'])->name('admin.store');
		Route::get('admin/index', [AdminController::class, 'index'])->name('admin.index');
		Route::delete('admin/delete/{id}', [AdminController::class, 'destroy'])->name('admin.delete');
		Route::get('admin/edit/{id}', [AdminController::class, 'edit'])->name('admin.edit');
		Route::post('admin/update/{id}', [AdminController::class, 'update'])->name('admin.update');
		  
		Route::get('user/create', [UserController::class, 'create'])->name('user.create');
		Route::post('user/store', [UserController::class, 'store'])->name('user.store');
		Route::get('user/index', [UserController::class, 'index'])->name('user.index');
		Route::delete('user/delete/{id}', [UserController::class, 'destroy'])->name('user.delete');
		Route::get('user/edit/{id}', [UserController::class, 'edit'])->name('user.edit');
		Route::post('user/update/{id}', [UserController::class, 'update'])->name('user.update');  
		  
		  
		 Route::get('department/create', [DepartmentController::class, 'create'])->name('department.create');
		Route::post('department/store', [DepartmentController::class, 'store'])->name('department.store');
		Route::get('department/index', [DepartmentController::class, 'index'])->name('department.index');
		Route::delete('department/delete/{id}', [DepartmentController::class, 'destroy'])->name('department.delete');
		Route::get('department/edit/{id}', [DepartmentController::class, 'edit'])->name('department.edit');
		Route::post('department/update/{id}', [DepartmentController::class, 'update'])->name('department.update');
		
		 		 
		Route::get('task/create', [TaskController::class, 'create'])->name('task.create');
		Route::post('task/store', [TaskController::class, 'store'])->name('task.store');
		Route::get('task/index', [TaskController::class, 'index'])->name('task.index');
		Route::delete('task/delete/{id}', [TaskController::class, 'destroy'])->name('task.delete');
		Route::get('task/edit/{id}', [TaskController::class, 'edit'])->name('task.edit');
		Route::post('task/update/{id}', [TaskController::class, 'update'])->name('task.update');
		 
		 
		 	Route::get('searchusers', [UserController::class, 'searchByName'])->name('searchusers.search');
			Route::get('getuser/{id}', [UserController::class, 'getUserdata'])->name('get.user');
			  
});
 
 
 

Route::get('/employee/login',[LoginController::class,'empLogin'])->name('admin.employeeLogin');
Route::post('/employee/login',[LoginController::class,'postEmpLogin'])->name('admin.postemplogin');
 
 	Route::group(['middleware' => ['auth:web']], function () {
 
			Route::get('/employee',[EmpDashboardController::class,'show'])->name('emp.dashboard');
		Route::get('employee/tasks', [TaskController::class, 'tasksEmp'])->name('emp.taskemp');
		Route::get('employee/task/{id}', [TaskController::class, 'editStatus'])->name('emp.changetaskedit');
		Route::post('employee/task/{id}', [TaskController::class, 'updateStatus'])->name('emp.changetaskupdate');
	});
	
	
	
	
	
	
	

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');
});
