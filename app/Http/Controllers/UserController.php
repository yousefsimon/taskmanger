<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Models\Doctor;
use App\Models\Subject;
use App\Models\User;
use App\Models\Address;
use App\Models\Department;
 use Illuminate\Validation\Rule; //import Rule class 


use Hash;



class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
       $users = User:: paginate(10);
	 
       return Inertia::render('user/index',[ 'users'=>$users  ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    { 
		$departments = Department::all();
	   return Inertia::render('user/create',['departments'=>$departments->toArray()]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
          $data	= $request->all();
		  $validatedData = $request->validate([
			'email' => Rule::unique('users') //use it in PUT or PATCH method

		]);
		
		$arrData =  [
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			'email' => $data['email'],
			'phone' => $data['phone'],
		    'salary' => $data['salary'],
			'department_id' => $data['department_id'],
			'password' => Hash::make($data['password']),
			'main_image_id' => $data['main_image_id'] 
		  ];
	 
		User::create($arrData);
		return to_route('user.index');
    }

 
    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
              $user=User::find($id);
			  $departments = Department::all();
			  
			  
			  		 $mainImg ="";
		
		if(isset($user->mainImage)){
			$mainImg = $user->mainImage->path;
		 }
		 
		  $user->img_path= $mainImg;
		 
		  return Inertia::render('user/edit',[ 'user'=>$user  , 'departments'=>$departments->toArray()  ]);
    }

  

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
           $admin=User::find($id);
			$admin->delete();
			return to_route('user.index');
    }
	
	
	
    /**
     * Remove the specified resource from storage.
     */
    public function searchByName(Request $request)
    {
			$term = $request->get('term');
           
 		    $users = User::where('first_name', 'like', $term.'%')->limit(10)->get();
			 
			  return response()->json([
							'data' =>$users ,  
				]);	
				
    }
	
	
	
	
	
    /**
     * Remove the specified resource from storage.
     */
    public function getUserdata($id)
    { 
 		     $user = User::find($id);
			 
			 
			return response()->json([
							'data' =>$user ,   
				]);	
				
    }
	
	
	
	
	
    /**
     * Update the specified resource in storage.
     */
    public function update($id,Request $request)
    {
		  $data	= $request->all();
		  $validatedData = $request->validate([
			'email' => 'required|email', // Example validation rules
		]);
		
		$arrData =  [
			'first_name' => $data['first_name'],
			'last_name' => $data['last_name'],
			 
			'phone' => $data['phone'],
		    'salary' => $data['salary'],
			'department_id' => $data['department_id'], 
			'main_image_id' => $data['main_image_id'] 
		  ];
	  
		$user = User::find($id); 
		$user->update($arrData);
		return to_route('user.index');
    }

}
