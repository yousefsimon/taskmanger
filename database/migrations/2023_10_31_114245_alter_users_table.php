<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
           Schema::table('users', function (Blueprint $table) {
					 
					 $table->integer('department_id')->unsigned()->nullable();
					 $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
					 $table->integer('salary')->nullable();
					 $table->integer('main_image_id')->unsigned()->nullable();
					 $table->foreign('main_image_id')->references('id')->on('media_images')->onDelete('cascade');
			});
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
